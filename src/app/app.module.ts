import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HelloComponent } from './hello/hello.component';
import { MessageComponent } from './message/message.component';
import { MycheckService } from './mycheck.service';

@NgModule({
  declarations: [
    AppComponent,
    HelloComponent,
    MessageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { 
  constructor( service : MycheckService){
    service.push("Taro");
    service.push("Hanako");
    service.push("Sachiko");
  }
}
