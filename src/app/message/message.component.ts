import { Component, OnInit, Input, Output,EventEmitter } from '@angular/core';
import { MycheckService } from '../mycheck.service';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent implements OnInit {

  @Input() content : string[];
  @Output() action = new EventEmitter<MouseEvent>();

  constructor(private service : MycheckService) {
    this.content = [];
   }

  ngOnInit(): void {
    this.content = this.service.list;
  }

  doAction(event){
    this.action.emit(event);
  }

  pop(){
    this.content.pop();
  }

}
